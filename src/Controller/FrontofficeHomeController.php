<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Circuit;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FrontofficeHomeController extends AbstractController
{
    /**
     * @Route("/home", name="front_home")
     */
    public function index()
    {

        return $this->render('front/home.html.twig', [
            'controller_name' => 'FrontofficeHomeController',
        ]);
    }

    /**
     * @Route("/list", name="front_list")
     */
    public function list()
    {
    	$em = $this->getDoctrine()->getManager();
		$circuits = $em->getRepository(Circuit::class)->findAll();

		dump($circuits);

		return $this->render('front/list.html.twig', [
		  'circuits' => $circuits, 
		]);
    }

	/**
     * Finds and displays a circuit entity.
     *
     * @Route("/circuit/{id}", name="front_circuit_show")
     */
    public function circuitShow($id)
    {
        $em = $this->getDoctrine()->getManager();
        $circuit = $em->getRepository(Circuit::class)->find($id);
        if(!$circuit){
            return $this->render('front/home.html.twig', [
            'controller_name' => 'FrontofficeHomeController',
        ]);
        }
        dump($circuit);
        return $this->render('front/circuit_show.html.twig', [
            'circuit' => $circuit,
        ]);
    }
}
